import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:localization_test/models/localization_manager.dart';

class LocalizationSettingsPage extends StatefulWidget {
  @override
  _LocalizationSettingsPageState createState() =>
      _LocalizationSettingsPageState();
}

class _LocalizationSettingsPageState extends State<LocalizationSettingsPage> {

  String selectedLanguageKey;
  String selectedRegionKey;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final localization = Provider.of<LocalizationManager>(context);
    selectedLanguageKey = localization.languageKey;
    selectedRegionKey = localization.regionKey;
  }

  @override
  Widget build(BuildContext context) {
    final localization = Provider.of<LocalizationManager>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(localization.text('localization_settings_title')),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 30,
                    child: Center(
                      child: Text(localization.text('languages'))
                    ),
                  ),
                  Expanded(
                    child: _buildLanguageListView(),
                    flex: 1,
                  )
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 30,
                    child: Center(
                      child: Text(localization.text('regions'))
                    ),
                  ),
                  Expanded(
                    child: _buildRegionListView(),
                    flex: 1,
                  )
                ],
              ),
            )
          ],
        ));
  }

  Widget _buildLanguageListView() {
    return ListView.builder(
        itemCount: LocalizationManager.supportedLanguages.keys.length,
        itemBuilder: (context, index) {
          final languageKey = LocalizationManager.supportedLanguages.keys.toList()[index];
          return _buildLanguageItem(languageKey);
        });
  }

  Widget _buildLanguageItem(String languageKey) {
    final localization = Provider.of<LocalizationManager>(context);

    return ListTile(
      onTap: () {
        selectedLanguageKey = languageKey;
        localization.languageKey = languageKey;
        localization.reloadText();
      },
      title: Text(
        LocalizationManager.supportedLanguages[languageKey]['name'],
        style: TextStyle(
          fontSize: 20.0,
        ),
      ),
      trailing: (languageKey == selectedLanguageKey) ? Icon(Icons.check): null,
    );
  }

  Widget _buildRegionListView() {
    return ListView.builder(
        itemCount: LocalizationManager.supportedRegions.length,
        itemBuilder: (context, index) {
          final regionKey = LocalizationManager.supportedRegions.keys.toList()[index];
          return _buildRegionItem(regionKey);
        });
  }

  Widget _buildRegionItem(String regionKey) {
    final localization = Provider.of<LocalizationManager>(context);

    return ListTile(
      onTap: () {
        selectedRegionKey = regionKey;
        localization.regionKey = regionKey;
        Provider.of<LocalizationManager>(context).reloadText();
      },
      title: Text(
        LocalizationManager.supportedRegions[regionKey]['country_code'],
        style: TextStyle(
          fontSize: 20.0,
        ),
      ),
      trailing: (regionKey == selectedRegionKey) ? Icon(Icons.check): null,
    );
  }
}
