import 'package:flutter/widgets.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:localization_test/features/register/registrar/registrar_cn.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  @override
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterSubmitPressed) {
      yield RegisterLoading();

      final isSuccessful = await Registrar.register(
        firstName: event.firstName, 
        lastName: event.lastName,
        phone: event.phone,
        email: event.email,
      );

      if (isSuccessful) {
        yield RegisterSuccess();
      } else {
        yield RegisterFailure();
      }
    }
  }
}

// Events

abstract class RegisterEvent extends Equatable {
  RegisterEvent([List props = const[]]) : super(props);
}

class RegisterSubmitPressed extends RegisterEvent {
  final String firstName;
  final String lastName;
  final String phone;
  final String email;

  RegisterSubmitPressed({
    @required this.firstName,
    @required this.lastName,
    @required this.phone,
    @required this.email,
  }) : super([firstName, lastName, phone, email]);
}

// States

abstract class RegisterState extends Equatable {}

class RegisterInitial extends RegisterState {
  @override
  String toString() => 'RegisterInitial';
}

class RegisterLoading extends RegisterState {
  @override
  String toString() => 'RegisterLoading';
}

class RegisterSuccess extends RegisterState {
  @override
  String toString() => 'RegisterSuccess';
}

class RegisterFailure extends RegisterState {
  @override
  String toString() => 'RegisterFailure';
}