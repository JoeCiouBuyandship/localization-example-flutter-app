import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'package:localization_test/models/localization_manager.dart';
import 'register_form_tw.dart' as tw;
import 'register_form_hk.dart' as hk; 
import 'register_form_cn.dart' as cn;
import 'package:localization_test/features/register/register_bloc/register_bloc_tw.dart' as tw;
import 'package:localization_test/features/register/register_bloc/register_bloc_hk.dart' as hk;
import 'package:localization_test/features/register/register_bloc/register_bloc_cn.dart' as cn;

class RegisterForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final localization = Provider.of<LocalizationManager>(context);

    switch (localization.regionKey) {
      case TaiwanRegionKey:
        return BlocProvider(
          builder: (context) {
            return tw.RegisterBloc();
          },
          child: tw.RegisterForm(),
        );
      case HongKongRegionKey:
        return BlocProvider(
          builder: (context) {
            return hk.RegisterBloc();
          },
          child: hk.RegisterForm(),
        );
      case ChinaRegionKey:
        return BlocProvider(
          builder: (context) {
            return cn.RegisterBloc();
          },
          child: cn.RegisterForm(),
        );
    }
    return null;
  }
}