import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localization_test/features/register/register_bloc/register_bloc_cn.dart';
import 'package:provider/provider.dart';

import 'package:localization_test/models/localization_manager.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final localization = Provider.of<LocalizationManager>(context);
    final registerBloc = BlocProvider.of<RegisterBloc>(context);

    return BlocListener(
      bloc: registerBloc,
      listener: (context, state) {
        Scaffold.of(context).hideCurrentSnackBar(reason: SnackBarClosedReason.timeout);
        if (state is RegisterSuccess) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(localization.text('register_success_message')),
              backgroundColor: Colors.green,
            ),
          );
        } else if (state is RegisterFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(localization.text('register_failure_message')),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      child: BlocBuilder(
        bloc: registerBloc,
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: _firstNameController,
                    decoration: InputDecoration(
                      labelText: localization.text('first_name'),
                    ),
                  ),
                  TextFormField(
                    controller: _lastNameController,
                    decoration: InputDecoration(
                      labelText: localization.text('last_name'),
                    ),
                  ),
                  TextFormField(
                    controller: _phoneController,
                    decoration: InputDecoration(
                      labelText: localization.text('phone'),
                    ),
                  ),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      labelText: localization.text('email'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: ButtonTheme(
                      minWidth: 120,
                      height: 44,
                      child: RaisedButton(
                        color: Colors.blue,
                        onPressed: () {
                          if (!(state is RegisterLoading)) {
                            registerBloc.dispatch(RegisterSubmitPressed(
                                firstName: _firstNameController.text,
                                lastName: _lastNameController.text,
                                phone: _phoneController.text,
                                email: _emailController.text));
                          }
                        },
                        child: state is RegisterLoading
                            ? SizedBox(
                                width: 25,
                                height: 25,
                                child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white)),
                              )
                            : Text(localization.text('submit'),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
