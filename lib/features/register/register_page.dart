import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:localization_test/models/localization_manager.dart';
import 'package:localization_test/features/register/register_form/register_form.dart';
import 'package:localization_test/features/localization_settings/localization_settings_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    final localization = Provider.of<LocalizationManager>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(localization.text('home_title')),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => LocalizationSettingsPage()));
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Text(localization.regionKey),
          RegisterForm()
        ],
      ),
    );
  }
}