import 'package:flutter/widgets.dart';

class Registrar {
  static Future<bool> register({
    @required String firstName,
    @required String lastName,
    @required String phone,
    @required String email,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    if (firstName.isEmpty) { return false; }
    if (lastName.isEmpty) { return false; }
    if (phone.length != 11) { return false; }
    if (email.isEmpty) { return false; }
    return true;
  }
}