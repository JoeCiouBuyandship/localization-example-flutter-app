import 'package:flutter/widgets.dart';

class Registrar {
  static Future<bool> register({
    @required String firstName,
    @required String lastName,
    @required String phone,
    @required String address,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    if (firstName.isEmpty) { return false; }
    if (lastName.isEmpty) { return false; }
    if (phone.length != 10) { return false; }
    if (address.isEmpty) { return false; }
    return true;
  }
}