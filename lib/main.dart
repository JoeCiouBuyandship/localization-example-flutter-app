import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:localization_test/models/localization_manager.dart';
import 'package:localization_test/features/register/register_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LocalizationManager>(
      builder: (context) => LocalizationManager(),
      child: MaterialApp(
        title: 'Localization Test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: RegisterPage(),
      )
    );
  }
}
