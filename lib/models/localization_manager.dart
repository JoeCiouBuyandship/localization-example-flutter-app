import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

const TaiwanRegionKey = 'tw';
const HongKongRegionKey = 'hk';
const ChinaRegionKey = 'cn';

const TraditionalChineseLanguageKey = 'zh_Hant';
const SimplifiedChineseLanguageKey = 'zh_Hans';
const EnglishLanguageKey = 'en';

class LocalizationManager with ChangeNotifier{

  static final supportedRegions = {
    TaiwanRegionKey: {
      'name': '台灣',
      'country_code': 'TW'
    },
    HongKongRegionKey: {
      'name': '香港',
      'country_code': 'HK'
    }, 
    ChinaRegionKey: {
      'name': '中國',
      'country_code': 'CN'
    }
  };

  static final supportedLanguages = {
    TraditionalChineseLanguageKey: {
      'name': '繁體中文',
      'language_code': 'zh',
      'script_code': 'Hant',
    }, 
    SimplifiedChineseLanguageKey: {
      'name': '简体中文',
      'language_code': 'zh',
      'script_code': 'Hans',
    }, 
    EnglishLanguageKey: {
      'name': 'English',
      'language_code': 'en',
    }
  };
  
  String languageKey = 'zh_Hant';
  String regionKey = 'tw';
  Map<dynamic, dynamic> _localizationText = {};

  LocalizationManager() {
    _loadText();
  }

  reloadText() async {
    await _loadText();
  }
  
  _loadText() async {
    String fileName = 'localization_' + languageKey + '.json'; 
    String jsonContent = await rootBundle.loadString('assets/locale/${fileName}');
    _localizationText = json.decode(jsonContent);
    notifyListeners();
  }
  
  String text(String key) {
    return _localizationText[key] ?? '$key not found';
  }
}
